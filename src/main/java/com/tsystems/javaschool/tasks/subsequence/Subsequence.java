package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // Check for valid arguments
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        int lastY = 0;
        // For each element in x look for match in y
        for (int i = 0; i < x.size(); i++) {
            // If match is found, remember index in y and start from it next time (j=lastY)
            // If no match is found for given x element, return false;
            boolean found = false;
            for (int j = lastY; j < y.size(); j++) {
                if (x.get(i).equals(y.get(j))) {
                    found = true;
                    lastY = ++j;
                    break;
                }
            }
            if (!found) return false;
        }

        return true;
    }
}