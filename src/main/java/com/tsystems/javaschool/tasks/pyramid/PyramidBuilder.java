package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // If something is wrong, throw exception
        try {

            // --- Algorithm starts here

            // Check if number of inputs is acceptable
            // Loop through all acceptable number of elements for such pyramids
            int N = inputNumbers.size();
            int r = 1; // number of elements if last row / number of rows
            int n = 1; // number of elements in pyramid
            boolean sizeOK = false;
            while (n <= N) {
                if (n == N) { // if match, stop counting
                    sizeOK = true;
                    break;
                } else {
                    r++;
                    n += r;
                }
            }
            if (!sizeOK) throw new CannotBuildPyramidException();


            // Sort inputs
            Collections.sort(inputNumbers);
            // Organize inputs as stack to simplify extraction
            Deque<Integer> stack = new ArrayDeque<>(inputNumbers);

            // Fill final array

            // Start from first row, initial offset such that first element in the middle
            // Stop filling row when there are enough elements times 2 because of gaps (offset+i*2+1)
            // When row is ready, decrease offset by 1
            int[][] result = new int[r][r * 2 - 1];
            int offset = r - 1;
            for (int i = 0; i < r; i++) {
                for (int j = offset; j < offset + i * 2 + 1; j += 2) {
                    result[i][j] = stack.pop();
                }
                offset--;
            }

            return result;

            // --- Algorithm ends here ---

        } catch (Throwable t) {
            throw new CannotBuildPyramidException();
        }
    }
}